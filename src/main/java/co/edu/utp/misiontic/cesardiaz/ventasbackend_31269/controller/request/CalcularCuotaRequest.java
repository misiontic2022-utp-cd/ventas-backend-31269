package co.edu.utp.misiontic.cesardiaz.ventasbackend_31269.controller.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CalcularCuotaRequest implements Serializable {
    
    private Integer monto;
    private Integer numeroCuotas;
    private String periodicidad;

}

package co.edu.utp.misiontic.cesardiaz.ventasbackend_31269;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentasBackend31269Application {

	public static void main(String[] args) {
		SpringApplication.run(VentasBackend31269Application.class, args);
	}

}

package co.edu.utp.misiontic.cesardiaz.ventasbackend_31269.controller.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CalcularCuotaResponse implements Serializable{
    
    private Integer valor;
    private String message;
}

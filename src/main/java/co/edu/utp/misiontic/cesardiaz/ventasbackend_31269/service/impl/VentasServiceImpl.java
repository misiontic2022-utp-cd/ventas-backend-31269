package co.edu.utp.misiontic.cesardiaz.ventasbackend_31269.service.impl;

import org.springframework.stereotype.Service;

import co.edu.utp.misiontic.cesardiaz.ventasbackend_31269.service.VentasService;

@Service
public class VentasServiceImpl implements VentasService {

    @Override
    public Integer calcularCuota(Integer monto, Integer numeroCuotas, String periodicidad) {
        if (monto == null || monto <= 0) {
            throw new RuntimeException("El monto no es válido");
        }
        if (numeroCuotas == null || numeroCuotas <= 0) {
            throw new RuntimeException("El número de cuotas no es válido");
        }
        
        var interes = calcularInteres(periodicidad);
        var total = monto * (100 + interes) / 100;
        Double cuota = Math.ceil(total / numeroCuotas);
        return cuota.intValue();
    }

    private Double calcularInteres(String periodicidad) {
        Double interes;
        switch(periodicidad) {
            case "Diario":
                interes = 1d;
                break;
            case "Semanal":
                interes = 2d;
                break;
            case "Quincenal":
                interes = 3d;
                break;
            case "Mensual":
                interes = 4d;
                break;
            default:
                throw new RuntimeException("La periodicidad no es válida");
        }
        return interes;
    }
    
}

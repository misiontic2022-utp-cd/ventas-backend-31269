package co.edu.utp.misiontic.cesardiaz.ventasbackend_31269.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.utp.misiontic.cesardiaz.ventasbackend_31269.controller.request.CalcularCuotaRequest;
import co.edu.utp.misiontic.cesardiaz.ventasbackend_31269.controller.response.CalcularCuotaResponse;
import co.edu.utp.misiontic.cesardiaz.ventasbackend_31269.service.VentasService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("api/ventas")
public class VentasController {

    private final VentasService service;

    @PostMapping("/cuota") // POST /api/sales/cuota
    public ResponseEntity<CalcularCuotaResponse> calcularValorCuota(
            @RequestBody CalcularCuotaRequest request) {
        try {
            var valor = service.calcularCuota(request.getMonto(), request.getNumeroCuotas(), request.getPeriodicidad());
            var respuesta = CalcularCuotaResponse.builder()
                    .valor(valor)
                    .build();
            return ResponseEntity.ok().body(respuesta);
        } catch (Exception ex) {
            return ResponseEntity.badRequest()
                    .body(CalcularCuotaResponse.builder()
                            .message(ex.getMessage())
                            .build());
        }
    }
}

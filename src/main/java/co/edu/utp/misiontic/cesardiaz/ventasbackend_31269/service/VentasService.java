package co.edu.utp.misiontic.cesardiaz.ventasbackend_31269.service;

public interface VentasService {

    Integer calcularCuota(Integer monto, Integer numeroCuotas, String periodicidad);
}
